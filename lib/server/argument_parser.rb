# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

require 'optparse'

require_relative 'config'

# The Server module
module Server
  # The ArgumentsParser class
  class ArgumentsParser
    attr_reader :parser, :options

    def initialize(option_parser = OptionParser.new)
      @parser = option_parser
      @options = ::Server.server_config.dup
      @flags = %i[banner port ssl idle_reading idle_writing log_requests log_level help version]
      @flags.each { |method_name| method(method_name)&.call if respond_to?(method_name) }
    end

    def banner
      @parser.banner = "Usage: #{File.basename($PROGRAM_NAME)} [port] [options]"
      @parser.separator ''
      @parser.separator 'Options:'
    end

    def validated_port(value, integer_pattern = /^\d+$/)
      raise OptionParser::InvalidArgument, "Invalid port: #{value}" unless \
        integer_pattern.match?(value.to_s) && value.positive? && value < 65_536

      value
    end

    def port
      description = "Port on which to listen for connections; default: #{@options[:port]}"
      @parser.on('-p', '--port=<port>', Integer, description) do |v|
        @options[:port] = validated_port(v).to_i
      end
    end

    def ssl
      @parser.on('-s', '--ssl', "Enable SSL socket server; default: #{@options[:ssl]}") do
        @options[:ssl] = true
      end
    end

    def idle_reading
      @parser.on('--idle-reading=seconds', 'Amount of time channel can idle without incoming data') do |v|
        @options[:idle_reading] = v.to_i
      end
    end

    def idle_writing
      @parser.on('--idle-writing=seconds', 'Amount of time channel can idle without outgoing data') do |v|
        @options[:idle_writing] = v.to_i
      end
    end

    def log_requests
      @parser.on('-r', '--log-requests', 'Include individual request info in log output') do
        @options[:log_requests] = true
      end
    end

    def log_level
      @parser.on_tail('-v', '--verbose', 'Increase verbosity') do
        current_level = @options.fetch(:log_level, 0)
        @options[:log_level] = current_level - 1
      end
    end

    def help
      @parser.on_tail('-?', '--help', 'Show this message') do
        puts @parser
        exit
      end
    end

    def version
      @parser.on_tail('--version', 'Show version') do
        puts "#{File.basename($PROGRAM_NAME)} version #{::Server::VERSION}"
        exit
      end
    end
  end
  # class ArgumentsParser

  def parse_arguments(arguments_parser = ::Server::ArgumentsParser.new)
    arguments_parser.parser.parse!(ARGV)
    arguments_parser.options
  rescue OptionParser::InvalidArgument, OptionParser::InvalidOption,
         OptionParser::AmbiguousOption => e
    abort e.message
  end
end
# module Server
