# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

require 'java'

# The Server module
module Server
  # The Listenable module
  module Listenable
    def listeners
      @listeners ||= java.util.concurrent.CopyOnWriteArrayList.new
    end

    def add_listener(*listener)
      listeners.addAll(listener)
    end

    def remove_listener(*listener)
      listeners.removeAll(listener)
    end

    def replace_listeners(*listener)
      listeners.clear
      add_listener(*listener)
    ensure
      log.trace "Listeners after replacement: #{listeners}"
    end

    def notify(event, *args)
      listeners.each do |listener|
        next unless listener.respond_to?(event.to_sym)
        log.trace "Notifying listener #{listener} of event: #{event}"
        listener.send(event.to_sym, *args)
      end
    end
  end
  # module Listenable
end
# module Server
