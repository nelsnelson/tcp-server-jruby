# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

require 'java'
require 'netty'

# The Server module
module Server
  java_import Java::io.netty.channel.ChannelFutureListener
  java_import Java::io.netty.channel.ChannelHandler
  java_import Java::io.netty.channel.SimpleChannelInboundHandler

  # The MessageHandler class
  class MessageHandler < SimpleChannelInboundHandler
    include ChannelHandler
    include ChannelFutureListener

    def initialize(&handler)
      super()
      @handler = handler
    end

    # Please keep in mind that this method will be renamed to
    # messageReceived(ChannelHandlerContext, I) in 5.0.
    #
    # java_signature 'protected abstract void channelRead0(ChannelHandlerContext ctx, I msg) throws Exception'
    def channelRead0(ctx, msg)
      log.trace "##{__method__} channel: #{ctx.channel}, message: #{msg.inspect}"
      messageReceived(ctx, msg)
    end

    def messageReceived(ctx, msg)
      log.trace "##{__method__} channel: #{ctx.channel}, message: #{msg.inspect}"
      msg&.chomp!
      log.info "Received message: #{msg}"
      return super(ctx, msg) unless respond_to?(:handle_message) && @handler&.arity == 2
      handle_message(ctx, msg)
    end

    def handle_message(ctx, message)
      request = message.to_s.strip
      response = @handler.call(ctx.channel, request).to_s.chomp
      log.debug "##{__method__} response: #{response}"
      ctx.channel.writeAndFlush("#{response}\n")
    end
  end
end
