# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

require 'java'
require 'netty'

require 'logger'

# The Server module
module Server
  DEFAULT_LOG_LEVEL = Logger::INFO
  DEFAULT_HOST = '0.0.0.0'.freeze
  DEFAULT_PORT = 8080
  DEFAULT_SSL_ENABLED = false
  DEFAULT_IDLE_READING_SECONDS = 5 * 60 # seconds
  DEFAULT_IDLE_WRITING_SECONDS = 30 # seconds
  DEFAULT_KEEP_ALIVE = false
  DEFAULT_MAX_QUEUED_INCOMING_CONNECTIONS = 100
  DEFAULT_MAX_FRAME_LENGTH = 8192
  DEFAULT_DELIMITER = Java::io.netty.handler.codec.Delimiters.lineDelimiter
  DEFAULT_LOG_REQUESTS = false
  DEFAULT_QUIT_COMMANDS = %i[
    bye cease desist exit leave quit stop terminate
  ].freeze

  # rubocop: disable Metrics/MethodLength
  def server_config
    @server_config ||= {
      host: DEFAULT_HOST,
      port: DEFAULT_PORT,
      ssl: DEFAULT_SSL_ENABLED,
      idle_reading: DEFAULT_IDLE_READING_SECONDS,
      idle_writing: DEFAULT_IDLE_WRITING_SECONDS,
      max_queued_incoming_connections: DEFAULT_MAX_QUEUED_INCOMING_CONNECTIONS,
      log_requests: DEFAULT_LOG_REQUESTS,
      log_level: DEFAULT_LOG_LEVEL,
      max_frame_length: DEFAULT_MAX_FRAME_LENGTH,
      keep_alive: DEFAULT_KEEP_ALIVE,
      delimiter: DEFAULT_DELIMITER,
      quit_commands: DEFAULT_QUIT_COMMANDS
    }.freeze
  end
  module_function :server_config
  # rubocop: enable Metrics/MethodLength
end
