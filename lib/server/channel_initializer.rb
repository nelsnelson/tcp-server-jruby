# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

require 'java'
require 'netty'

require_relative 'message_handler'
require_relative 'modular_handler'

# The Server module
module Server
  java_import Java::io.netty.handler.codec.DelimiterBasedFrameDecoder
  java_import Java::io.netty.handler.codec.Delimiters
  java_import Java::io.netty.handler.codec.string.StringDecoder
  java_import Java::io.netty.handler.codec.string.StringEncoder
  java_import Java::io.netty.handler.ssl.SslContextBuilder
  java_import Java::io.netty.handler.ssl.SslHandler
  java_import Java::io.netty.handler.ssl.util.InsecureTrustManagerFactory
  java_import Java::io.netty.handler.ssl.util.SelfSignedCertificate
  java_import Java::io.netty.util.concurrent.FutureListener

  # The ChannelInitializer class
  class ChannelInitializer < Java::io.netty.channel.ChannelInitializer
    attr_accessor :decoder, :encoder, :user_handlers
    attr_reader :options

    def initialize(channel_group, options = {})
      super()
      @channel_group = channel_group
      @options = options
      @user_handlers = options.fetch(:handlers, [])
      @decoder = StringDecoder.new
      @encoder = StringEncoder.new
    end

    def <<(handler)
      @user_handlers << handler
    end

    def initChannel(channel)
      pipeline = channel.pipeline
      pipeline.addLast(ssl_handler(channel)) if @options[:ssl]
      pipeline.addLast(frame_decoder, @decoder, @encoder)
      add_user_handlers(pipeline)
      pipeline.addLast(default_handler)
    end

    def frame_decoder
      DelimiterBasedFrameDecoder.new(@options[:max_frame_length], @options[:delimiter])
    end

    def default_handler
      @default_handler ||= ::Server::ModularHandler.new(@channel_group)
    end

    def add_listener(*listener)
      default_handler.add_listener(*listener)
    end

    def replace_listeners(*listener)
      default_handler.replace_listeners(*listener)
    end

    protected

    def add_user_handlers(pipeline)
      @user_handlers.each do |handler|
        case handler
        when Class then pipeline.addLast(handler.new)
        when Proc then pipeline.addLast(::Server::MessageHandler.new(&handler))
        else pipeline.addLast(handler)
        end
      end
    end

    private

    def ssl_context
      return @ssl_ctx unless @ssl_ctx.nil?
      log.debug 'Initializing SSL context'
      require 'bouncycastle'
      ssc = SelfSignedCertificate.new
      builder = SslContextBuilder.forServer(ssc.certificate(), ssc.privateKey())
      @ssl_ctx = builder.trustManager(InsecureTrustManagerFactory::INSTANCE).build()
    end

    def ssl_handler_instance(channel, ssl_ctx = ssl_context)
      return ssl_ctx.newHandler(channel.alloc()) if ssl_ctx.respond_to?(:newHandler)
      log.warn 'The SSL context did not provide a handler initializer'
      log.warn 'Creating handler with SSL engine of the context'
      SslHandler.new(ssl_engine(ssl_ctx))
    end

    def ssl_handler(channel)
      handler = ssl_handler_instance(channel)
      handler.handshake_future.addListener(ssl_handshake_future_listener)
      handler
    end

    def ssl_handshake_future_listener(listener = Object.new.extend(FutureListener))
      listener.define_singleton_method(:operationComplete) do |future|
        raise future.cause unless future.success?
        session = future.now.pipeline.get('SslHandler#0')&.engine&.session
        ::Server.log.info "Channel protocol: #{session.protocol}, cipher suite: #{session.cipher_suite}"
      rescue StandardError => e
        ::Server.log.warn "Error handling operation complete event: #{e.message}"
      end
      listener
    end

    def ssl_engine(ssl_ctx)
      ssl_engine = ssl_ctx.createSSLEngine()
      ssl_engine.setUseClientMode(false) # Server mode
      ssl_engine.setNeedClientAuth(false)
      ssl_engine
    end
  end
  # class ChannelInitializer
end
# module Server
