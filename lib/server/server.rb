# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

require 'java'
require 'netty'

require_relative 'instance_methods'
require_relative 'listenable'

# The Server module
module Server
  # The Server class sets up the netty server.
  class Server
    CHANNEL_TYPE = Java::io.netty.channel.socket.nio.NioServerSocketChannel.java_class
    include ::Server::InstanceMethods
    attr_reader :options

    def initialize(options = {}, *handlers, &block)
      raise ArgumentError, 'Parameter may not be nil: options' if options.nil?
      @options = ::Server.server_config.merge(options)
      configure_handlers(*(handlers.empty? && block.nil? ? [self] : handlers), &block)
    ensure
      listeners = channel_initializer.default_handler.listeners
      log.trace "##{__method__} listeners: #{listeners}"
    end
  end
  # class Server
end
# module Server
