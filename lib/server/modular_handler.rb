# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

require 'java'
require 'netty'

require_relative 'listenable'

# The Server module
module Server
  java_import Java::io.netty.channel.SimpleChannelInboundHandler

  # The ModularHandler class notifies listeners about events.
  class ModularHandler < SimpleChannelInboundHandler
    include ::Server::Listenable

    def initialize(channel_group)
      super()
      @channel_group = channel_group
    end

    def isSharable
      true
    end

    def channelRegistered(ctx)
      log.trace "##{__method__} channel: #{ctx.channel}"
      notify :channel_registered, ctx
      super(ctx)
    end

    def channelUnregistered(ctx)
      log.trace "##{__method__} channel: #{ctx.channel}"
      notify :channel_unregistered, ctx
      super(ctx)
    end

    def channelActive(ctx)
      ::Server.log.debug "##{__method__} channel: #{ctx.channel}"
      @channel_group.add(ctx.channel)
      notify :channel_active, ctx
      super(ctx)
    end

    def channelInactive(ctx)
      log.trace "##{__method__} channel: #{ctx.channel}"
      notify :channel_inactive, ctx
      super(ctx)
    end

    def channelRead(ctx, msg)
      log.trace "##{__method__} channel: #{ctx.channel}, message: #{msg.inspect}"
      notify :channel_read, ctx, msg
      super(ctx, msg)
    end

    # Please keep in mind that this method will be renamed to
    # messageReceived(ChannelHandlerContext, I) in 5.0.
    #
    # java_signature 'protected abstract void channelRead0(ChannelHandlerContext ctx, I msg) throws Exception'
    def channelRead0(ctx, msg)
      log.trace "##{__method__} channel: #{ctx.channel}, message: #{msg.inspect}"
      messageReceived(ctx, msg)
      # super(ctx, msg)
    end

    def messageReceived(ctx, msg)
      log.trace "##{__method__} channel: #{ctx.channel}, message: #{msg.inspect}"
      notify :message_received, ctx, msg
      # super(ctx, msg)
    end

    def channelReadComplete(ctx)
      log.trace "##{__method__} channel: #{ctx.channel}"
      notify :channel_read_complete, ctx
      super(ctx)
    end

    def userEventTriggered(ctx, evt)
      log.trace "##{__method__} channel: #{ctx.channel}, event: #{evt}"
      notify :user_event_triggered, ctx, evt
      super(ctx, evt)
    end

    def channelWritabilityChanged(ctx)
      log.trace "##{__method__} channel: #{ctx.channel}"
      notify :channel_writability_changed, ctx
      super(ctx)
    end

    def exceptionCaught(ctx, cause)
      ::Server.log.warn "##{__method__} channel: #{ctx.channel}, cause: #{cause.message}"
      cause.backtrace.each { |t| ::Server.log.error t }
      listeners = notify :exception_caught, ctx, cause
      super(ctx, cause) if listeners.nil? || listeners.empty?
    end

    IdentifierTemplate = '#<%<class>s:0x%<id>s>'.freeze

    def to_s
      format(IdentifierTemplate, class: self.class.name, id: object_id.to_s(16))
    end
    alias inspect to_s
  end
  # class ModularHandler
end
# module Server
