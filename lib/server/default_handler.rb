# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

# The Server module
module Server
  # The DefaultHandler module
  module DefaultHandler
    def channel_active(ctx)
      ::Server.log.info "Channel active: #{ctx.channel}"
      response = 'Hello, world!'
      log.trace "Sending response: #{response.inspect}"
      ctx.channel.writeAndFlush("#{response}\n")
    end

    # rubocop: disable Metrics/AbcSize
    def message_received(ctx, msg)
      return if msg.nil?
      msg.chomp! if msg.respond_to?(:chomp!)
      return if msg.respond_to?(:empty?) && msg.empty?
      log.trace "##{__method__} channel: #{ctx.channel}, message: #{msg.inspect}"
      return ctx.close() if @options[:quit_commands].include?(msg.downcase.to_sym)
      response = msg.upcase
      log.debug "Sending response: #{response.inspect}"
      ctx.writeAndFlush("#{response}\n")
    end
    # rubocop: enable Metrics/AbcSize

    def exception_caught(_ctx, cause)
      ::Server.log.error "Exception caught: #{cause}"
      cause.backtrace.each { |t| ::Server.log.error t }
      ctx.close()
    end
  end
end
