# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

require 'java'
require 'netty'

require_relative 'channel_initializer'
require_relative 'default_handler'
require_relative 'shutdown_hook'

# The Server module
module Server
  java_import Java::io.netty.bootstrap.ServerBootstrap
  java_import Java::io.netty.channel.ChannelOption
  java_import Java::io.netty.channel.group.DefaultChannelGroup
  java_import Java::io.netty.channel.nio.NioEventLoopGroup
  java_import Java::io.netty.handler.logging.LogLevel
  java_import Java::io.netty.handler.logging.LoggingHandler
  java_import Java::io.netty.util.concurrent.GlobalEventExecutor

  # The InstanceMethods module
  module InstanceMethods
    include ::Server::DefaultHandler

    # rubocop: disable Metrics/AbcSize
    def bootstrap
      @bootstrap = ServerBootstrap.new
      @bootstrap.group(boss_group, worker_group)
      @bootstrap.channel(channel_type)
      @bootstrap.option(ChannelOption::SO_BACKLOG, max_queued_incoming_connections)
      @bootstrap.childOption(ChannelOption::SO_KEEPALIVE, keep_alive) if keep_alive
      @bootstrap.handler(logging_handler) if options.fetch(:log_requests, false)
      @bootstrap.childHandler(channel_initializer)
    end
    # rubocop: enable Metrics/AbcSize

    def configure_handlers(*handlers, &block)
      channel_initializer << block if block_given?
      add_listener(*handlers)
    end

    def channel_type
      @channel_type ||= Server::CHANNEL_TYPE
    end

    def channel_initializer
      @channel_initializer ||= ::Server::ChannelInitializer.new(channel_group, @options)
    end

    def boss_group
      @boss_group ||= NioEventLoopGroup.new(2)
    end

    def worker_group
      @worker_group ||= NioEventLoopGroup.new
    end

    def channel_group
      @channel_group ||= DefaultChannelGroup.new('server_channels', GlobalEventExecutor::INSTANCE)
    end

    def logging_handler
      @logging_handler ||= LoggingHandler.new(LogLevel::INFO)
    end

    # rubocop: disable Metrics/AbcSize
    # rubocop: disable Metrics/MethodLength
    def run(port = self.port)
      channel = bootstrap.bind(port).sync().channel()
      channel_group.add(channel)
      ::Server::ShutdownHook.new(self)
      log.info "Listening on #{channel.local_address}"
      channel.closeFuture().sync()
    rescue java.lang.IllegalArgumentException => e
      raise "Invalid argument: #{e.message}"
    rescue java.net.BindException => e
      raise "Bind error: #{e.message}: #{options[:host]}:#{port}"
    rescue java.net.SocketException => e
      raise "Socket error: #{e.message}: #{options[:host]}:#{port}"
    ensure
      stop
    end
    # rubocop: enable Metrics/AbcSize
    # rubocop: enable Metrics/MethodLength

    def port
      @port ||= (@options[:port] || DEFAULT_PORT).to_i
    end

    def shutdown
      channel_group.disconnect().awaitUninterruptibly()
      channel_group.close().awaitUninterruptibly()
    end

    def stop
      boss_group&.shutdownGracefully()
      worker_group&.shutdownGracefully()
    end

    def <<(handler)
      channel_initializer << handler
    end

    def add_listener(*listener)
      channel_initializer.add_listener(*listener)
    end

    def replace_listeners(*listener)
      channel_initializer.replace_listeners(*listener)
    end

    def keep_alive
      @keep_alive ||= begin
        value = @options[:keep_alive]
        value || DEFAULT_KEEP_ALIVE
      end
    end

    def max_queued_incoming_connections
      @max_queued_incoming_connections ||= begin
        value = @options[:max_queued_incoming_connections]
        value = DEFAULT_MAX_QUEUED_INCOMING_CONNECTIONS if value.nil?
        value.to_java(java.lang.Integer)
      end
    end
  end
  # module ServerInstanceMethods
end
# module Server
