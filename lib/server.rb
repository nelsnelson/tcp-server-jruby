#! /usr/bin/env jruby

# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

require_relative 'demo_listener'
require_relative 'logging'
require_relative 'server/argument_parser'
require_relative 'server/server'

# The Server module
module Server
  def main(args = parse_arguments)
    Logging.log_level = args[:log_level]
    demo_handler = ::Server::Demo.new(args)
    ::Server::Server.new(args, demo_handler).run
  rescue Interrupt => e
    warn format("\r%<class>s", class: e.class)
    exit
  rescue StandardError => e
    ::Server.log.fatal(e)
    e.backtrace.each { |t| ::Server.log.fatal t }
    exit 1
  end
end

Object.new.extend(Server).main if $PROGRAM_NAME == __FILE__
