#! /usr/bin/env jruby

# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

require 'java'
require 'optparse'

require 'netty'

require_relative 'logging'

# The Client module
module Client
  def client_config
    @client_config ||= {
      host: '0.0.0.0',
      port: 8080,
      ssl: false,
      log_level: Logger::INFO,
      quit_commands: %i[bye cease desist exit leave quit stop terminate],
      max_frame_length: 8192,
      delimiter: Java::io.netty.handler.codec.Delimiters.lineDelimiter
    }.freeze
  end
  module_function :client_config
end

# The Client module
module Client
  java_import Java::io.netty.bootstrap.Bootstrap
  java_import Java::io.netty.channel.ChannelOption
  java_import Java::io.netty.channel.nio.NioEventLoopGroup
  java_import Java::io.netty.handler.ssl.SslContextBuilder
  java_import Java::io.netty.handler.ssl.util.InsecureTrustManagerFactory

  # The InitializationMethods module
  module InitializationMethods
    def init(options)
      @options = options
      @host = @options[:host]
      @port = @options[:port]
      @queue = java.util.concurrent.LinkedBlockingQueue.new
    end

    def bootstrap
      @bootstrap = Bootstrap.new
      @bootstrap.group(client_group)
      @bootstrap.channel(::TCP::CHANNEL_TYPE)
      @bootstrap.option(ChannelOption::TCP_NODELAY, true)
      @bootstrap.handler(logging_handler) if @options[:log_requests]
      @bootstrap.handler(channel_initializer)
    end

    def client_group
      @client_group ||= NioEventLoopGroup.new
    end

    def channel_initializer
      @channel_initializer ||= ::Client::ChannelInitializer.new(@options)
    end

    def logging_handler
      @logging_handler ||= LoggingHandler.new(LogLevel::INFO)
    end

    def configure_handlers(*handlers, &block)
      channel_initializer.default_handler.add_listener(self)
      channel_initializer.default_handler.listeners.addAll(handlers)
      @user_app = block
      @application_handler = lambda do |ctx, msg|
        if @user_app.nil? || @user_app.arity == 1
          @queue.add(msg.chomp)
        else
          @user_app.call(ctx, msg)
        end
      end
    end
  end
  # module InitializationMethods
end
# module Client

# The Client module
module Client
  java_import Java::io.netty.channel.AbstractChannel

  # The InstanceMethods module
  module InstanceMethods
    def puts(msg)
      wait_until_channel_is_active
      msg.chomp!
      log.trace "#puts msg: #{msg.inspect}"
      raise 'Message is empty!' if msg.nil? || msg.empty?
      @last_write_future = @channel.writeAndFlush("#{msg}\n")
    end

    def gets
      log.debug 'Waiting for response from server'
      @queue.take
    rescue StandardError => e
      warn "Unexpected error waiting for message: #{e.message}"
      nil
    end

    def wait_until_channel_is_active(timeout = 5, give_up = Time.now + timeout)
      sleep 0.1 until @channel.active? || Time.now > give_up
    end

    def connect(host = @options[:host], port = @options[:port])
      return unless @channel.nil?
      # Start the connection attempt.
      @channel = bootstrap.connect(host, port).sync().channel()
    rescue AbstractChannel::AnnotatedConnectException => e
      raise e.message
    rescue StandardError => e
      raise "Connection failure: #{e.message}"
    end

    def close(channel = @channel)
      log.debug "Closing client channel: #{channel}"
      channel.closeFuture().sync()
      # Wait until all messages are flushed before closing the channel.
      @last_write_future&.sync()
    ensure
      shutdown
    end

    def shutdown
      log.debug 'Shutting down gracefully'
      @client_group&.shutdownGracefully()
    ensure
      client_has_shut_down
    end

    def session
      when_client_has_shut_down(@client_group) do |group|
        log.debug "Channel group has shut down: #{group}"
      end
      @user_app.nil? ? read_user_commands : invoke_user_app
    end

    def invoke_user_app
      @user_app&.call(self)
    ensure
      close
    end

    def shut_down_callbacks
      @shut_down_callbacks ||= []
    end

    def when_client_has_shut_down(*args, &block)
      shut_down_callbacks << {
        block: block,
        args: args
      }
      self
    end

    def client_has_shut_down
      shut_down_callbacks.take_while do |callback|
        callback[:block]&.call(*callback.fetch(:args, []))
      end
    rescue StandardError => e
      log.error e.message
    end

    def channel_unregistered(ctx)
      log.trace "##{__method__} channel: #{ctx.channel}"
      # shutdown
    end

    def message_received(ctx, message)
      notify :message_received, ctx, message
      if @application_handler.nil?
        $stdout.print message.chomp unless message.nil?
      else
        @application_handler.call(ctx, message)
      end
    end

    def execute_command(str, client = self)
      return if str.empty?
      client.puts str
      close if @options[:quit_commands].include?(str.downcase.to_sym)
    end

    def read_user_commands
      log.trace 'Reading user commands'
      loop do
        log.debug 'Waiting for user input...'
        input = $stdin.gets
        raise 'Poll failure from stdin' if input.nil?
        break unless @channel.active?
        break unless execute_command(input).nil?
      end
    end

    IdentiferTemplate = '#<%<class>s:0x%<id>s @options=%<opts>s>'.freeze

    def to_s
      format(IdentiferTemplate, class: self.class.name, id: object_id.to_s(16), opts: @options.to_s)
    end
  end
  # module InstanceMethods
end
# module Client

# The Client module
module Client
  # The Listenable module
  module Listenable
    def listeners
      @listeners ||= java.util.concurrent.CopyOnWriteArrayList.new
    end

    def add_listener(*listener)
      listeners.addAll(listener)
    end

    def remove_listener(*listener)
      listeners.removeAll(listener)
    end

    def replace_listeners(*listener)
      listeners.clear
      add_listener(*listener)
    ensure
      log.trace "##{__method__} listeners: #{listeners}"
    end

    def notify(event, *args)
      listeners.each do |listener|
        next unless listener.respond_to?(event.to_sym)
        log.trace "Notifying listener #{listener} of event: #{event}"
        listener.send(event.to_sym, *args)
      end
    end
  end
  # module Listenable
end
# module Client

# The Client module
module Client
  # java_import Java::io.netty.channel.ChannelInboundHandlerAdapter
  java_import Java::io.netty.channel.SimpleChannelInboundHandler

  # The ModularHandler class
  # class ModularHandler < ChannelInboundHandlerAdapter
  class ModularHandler < SimpleChannelInboundHandler
    include ::Client::Listenable

    def isSharable
      true
    end

    def channelRegistered(ctx)
      log.trace "##{__method__} channel: #{ctx.channel}"
      notify :channel_registered, ctx
      super(ctx)
    end

    def channelUnregistered(ctx)
      log.trace "##{__method__} channel: #{ctx.channel}"
      notify :channel_unregistered, ctx
      super(ctx)
    end

    def channelActive(ctx)
      ::Client.log.debug "Channel active #{ctx.channel}"
      notify :channel_active, ctx
      super(ctx)
    end

    def channelInactive(ctx)
      log.trace "##{__method__} channel: #{ctx.channel}"
      notify :channel_inactive, ctx
      super(ctx)
    end

    def messageReceived(ctx, msg)
      log.trace "##{__method__} channel: #{ctx.channel}, message: #{msg.inspect}"
      notify :message_received, ctx, msg
    end

    def channelRead(ctx, msg)
      log.trace "##{__method__} channel: #{ctx.channel}, message: #{msg.inspect}"
      notify :channel_read, ctx, msg
      super(ctx, msg)
    end

    # Please keep in mind that this method will be renamed to
    # messageReceived(ChannelHandlerContext, I) in 5.0.
    #
    # java_signature 'protected abstract void channelRead0(ChannelHandlerContext ctx, I msg) throws Exception'
    def channelRead0(ctx, msg)
      log.trace "##{__method__} channel: #{ctx.channel}, message: #{msg.inspect}"
      messageReceived(ctx, msg)
    end

    def channelReadComplete(ctx)
      log.trace "##{__method__} channel: #{ctx.channel}"
      notify :channel_read_complete, ctx
      super(ctx)
    end

    def channelWritabilityChanged(ctx)
      log.trace "##{__method__} channel: #{ctx.channel}"
      notify :channel_writability_changed, ctx
      super(ctx)
    end

    def userEventTriggered(ctx, evt)
      log.trace "##{__method__} channel: #{ctx.channel}, event: #{evt}"
      notify :user_event_triggered, ctx, evt
      super(ctx, evt)
    end

    def exceptionCaught(ctx, cause)
      ::Client.log.warn "##{__method__} channel: #{ctx.channel}, cause: #{cause.message}"
      listeners = notify :exception_caught, ctx, cause
      super(ctx, cause) if listeners.empty?
    end

    IdentiferTemplate = '#<%<class>s:0x%<id>s>'.freeze

    def to_s
      format(IdentiferTemplate, class: self.class.name, id: object_id.to_s(16))
    end
    alias inspect to_s
  end
  # class ModularHandler
end
# module Client

# The Client module
module Client
  java_import Java::io.netty.handler.codec.DelimiterBasedFrameDecoder
  java_import Java::io.netty.handler.codec.Delimiters
  java_import Java::io.netty.handler.codec.string.StringDecoder
  java_import Java::io.netty.handler.codec.string.StringEncoder

  # The ChannelInitializer class
  class ChannelInitializer < Java::io.netty.channel.ChannelInitializer
    attr_accessor :decoder, :encoder, :handlers

    def initialize(options = {})
      super()
      @options = options
      @host = @options[:host]
      @port = @options[:port]
      @handlers = []
      @decoder = StringDecoder.new
      @encoder = StringEncoder.new
    end

    def <<(handler)
      @handlers << handler
    end

    def initChannel(channel)
      pipeline = channel.pipeline
      pipeline.addLast(ssl_handler(channel)) if @options[:ssl]
      # pipeline.addLast(frame_decoder)
      pipeline.addLast(decoder)
      pipeline.addLast(encoder)
      add_user_handlers(pipeline)
      pipeline.addLast(default_handler)
    end

    def frame_decoder
      DelimiterBasedFrameDecoder.new(@options[:max_frame_length], @options[:delimiter])
    end

    def default_handler
      @default_handler ||= ::Client::ModularHandler.new
    end

    protected

    def add_user_handlers(pipeline)
      @handlers.each do |handler|
        case handler
        when Class then pipeline.addLast(handler.new)
        when Proc then pipeline.addLast(Server::MessageHandler.new(&handler))
        else pipeline.addLast(handler)
        end
      end
    end

    private

    def ssl_context
      return @ssl_ctx unless @ssl_ctx.nil?
      log.debug 'Initializing SSL context'
      builder = SslContextBuilder.forClient()
      @ssl_ctx = builder.trustManager(InsecureTrustManagerFactory::INSTANCE).build()
    end

    def ssl_handler(channel, ssl_ctx = ssl_context)
      return ssl_ctx.newHandler(channel.alloc(), @host, @port) if ssl_ctx.respond_to?(:newHandler)
      log.warn 'The SSL context did not provide a handler initializer'
      log.warn 'Creating handler with SSL engine of the context'
      SslHandler.new(ssl_engine(ssl_ctx))
    end

    def ssl_engine(ssl_ctx)
      ssl_engine = ssl_ctx.createSSLEngine()
      ssl_engine.setUseClientMode(true) # Client mode
      ssl_engine.setNeedClientAuth(false)
      ssl_engine
    end
  end
  # class ChannelInitializer
end
# module Client

# The TCP module
module TCP
  CHANNEL_TYPE = Java::io.netty.channel.socket.nio.NioSocketChannel.java_class

  # The Client class
  class Client
    include ::Client::InitializationMethods
    include ::Client::InstanceMethods
    include ::Client::Listenable

    def initialize(options = {}, *handlers, &block)
      init(::Client.client_config.merge(options))
      configure_handlers(*handlers, &block)
      listeners = channel_initializer.default_handler.listeners
      log.trace "##{__method__} listeners: #{listeners}"
      connect
      session
    end

    IdentiferTemplate = '#<%<class>s:0x%<id>s>'.freeze

    def to_s
      format(IdentiferTemplate, class: self.class.name, id: object_id.to_s(16))
    end
    alias inspect to_s
  end
end
# module Client

# The Client module
module Client
  # The ArgumentsParser class
  class ArgumentsParser
    attr_reader :parser, :options

    def initialize(parser = OptionParser.new, options = ::Client.client_config.dup)
      @parser = parser
      @options = options
      @flags = %i[banner port ssl log_level help version]
      @flags.each { |method_name| method(method_name)&.call if respond_to?(method_name) }
    end

    def banner
      @parser.banner = "Usage: #{File.basename($PROGRAM_NAME)} [options] <hostname> [port]"
      @parser.separator ''
      @parser.separator 'Options:'
    end

    def host
      description = "Connect to server at this host; default: #{@options[:host]}"
      @parser.on('-h', '--hostname=<hostname>', description) do |v|
        @options[:host] = v
      end
    end

    def validated_port(val, integer_pattern = /^\d+$/)
      raise OptionParser::InvalidArgument, "Invalid port: #{val}" unless \
        integer_pattern.match?(val.to_s) && val.positive? && val < 65_536

      val
    end

    def port
      description = "Connect to server at this port; default: #{@options[:port]}"
      @parser.on('-p', '--port=<port>', Integer, description) do |v|
        @options[:port] = validated_port(v).to_i
      end
    end

    def ssl
      @parser.on('--ssl', "Secure connection with TLSv1.3; default: #{@options[:ssl]}") do
        @options[:ssl] = true
      end
    end

    def log_level
      @parser.on_tail('-v', '--verbose', 'Increase verbosity') do
        @options[:log_level] ||= 0
        @options[:log_level] -= 1
      end
    end

    def help
      @parser.on_tail('-?', '--help', 'Show this message') do
        puts @parser
        exit
      end
    end

    def version
      @parser.on_tail('--version', 'Show version') do
        puts "#{File.basename($PROGRAM_NAME)} version #{::Server::VERSION}"
        exit
      end
    end

    def parse_positional_arguments!
      @options[:host] = ARGV.shift or raise OptionParser::MissingArgument, 'hostname'
      return if (given_port = ARGV.shift&.to_i).nil?

      @options[:port] = validated_port(given_port).to_i
    end
  end
  # class ArgumentsParser

  def parse_arguments(arguments_parser = ArgumentsParser.new)
    arguments_parser.parser.parse!(ARGV)
    arguments_parser.parse_positional_arguments!
    arguments_parser.options
  rescue OptionParser::InvalidArgument, OptionParser::InvalidOption,
         OptionParser::MissingArgument, OptionParser::NeedlessArgument => e
    puts e.message
    puts parser
    exit
  rescue OptionParser::AmbiguousOption => e
    abort e.message
  end
end
# module Client

# The Client module
module Client
  # The Monitor class
  class Monitor
    def exception_caught(ctx, cause)
      log.warn "Unexpected exception from downstream channel #{ctx.channel}: #{cause.message}"
    end

    def channel_registered(ctx)
      log.info "Channel registered: #{ctx.channel}"
    end
  end
end
# module Client

# The Client module
module Client
  # The Console class
  class Console
    def message_received(ctx, message)
      log.trace "##{__method__} channel: #{ctx.channel}, message: #{message}"
      log.debug "Received message: #{message}"
      $stdout.print message unless message.nil?
    end
  end
end
# module Client

# The Client module
module Client
  # rubocop: disable Metrics/AbcSize
  # rubocop: disable Metrics/MethodLength
  def main(args = parse_arguments)
    Logging.log_level = args[:log_level]
    ::TCP::Client.new(args, ::Client::Console.new, ::Client::Monitor.new)
  rescue Interrupt => e
    warn format("\r%<class>s", class: e.class)
    exit
  rescue RuntimeError => e
    ::Client.log.fatal e.message
    exit 1
  rescue StandardError => e
    ::Client.log.fatal "Unexpected error: #{e.class}: #{e.message}"
    e.backtrace.each { |t| log.debug t } if Logging.log_level == Logger::DEBUG
    exit 1
  end
  # rubocop: enable Metrics/AbcSize
  # rubocop: enable Metrics/MethodLength
end
# module Client

Object.new.extend(Client).main if $PROGRAM_NAME == __FILE__
