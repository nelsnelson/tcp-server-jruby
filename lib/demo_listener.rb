# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

# =begin
#
# Copyright Nels Nelson 2016-2024 but freely usable (see license)
#
# =end

require_relative 'server/default_handler'

# The Server module
module Server
  # The Demo class
  class Demo
    include Server::DefaultHandler

    def initialize(options = {})
      @options = options
    end
  end
end
