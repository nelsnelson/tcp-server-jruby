FROM jruby:9.4.6.0

WORKDIR /usr/src/tcp-server-jruby

COPY . .
RUN bundle install \
  && \
  rm Gemfile.lock

CMD ["./tcp_server.rb"]
