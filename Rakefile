# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'rake'
require 'rake/clean'

PROJECT = File.basename(__dir__) unless defined?(PROJECT)

load "#{PROJECT}.gemspec"

CLEAN.add File.join('tmp', '**', '*'), 'tmp'
CLOBBER.add '*.gem', 'pkg'

task default: %i[package]

desc 'Run the rubocop linter'
task :lint do
  system('bundle', 'exec', 'rubocop') or abort
end

desc 'Run the spec tests'
task :test do
  system('bundle', 'exec', 'rspec', '--exclude-pattern', 'spec/verify/**/*_spec.rb') or abort
end
task test: :lint

desc 'Explode the gem'
task :explode do
  system('jgem', 'install', '--no-document', '--install-dir=tmp', '*.gem')
end
task explode: :clean

desc 'Package the gem'
task :package do
  system('jgem', 'build')
end
task package: %i[clean clobber test]

desc 'Verify the gem'
task :verify do
  system('bundle', 'exec', 'rspec', 'spec/verify') or abort
end
task verify: :explode

desc 'Publish the gem'
task :publish do
  system('jgem', 'push', latest_gem)
end
task publish: :verify

def latest_gem
  `ls -t #{PROJECT}*.gem`.strip.split("\n").first
end
