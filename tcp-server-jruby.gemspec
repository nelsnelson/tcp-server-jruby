# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

require_relative 'lib/server/version'

PROJECT = File.basename(__dir__) unless defined?(PROJECT)
JRUBY_PROJECT_SUFFIX = %r{-jruby} unless defined?(JRUBY_PROJECT_SUFFIX)
EMPTY_STRING = ''.freeze unless defined?(EMPTY_STRING)

# rubocop: disable Gemspec/RequiredRubyVersion
# rubocop: disable Metrics/AbcSize
# rubocop: disable Metrics/MethodLength
def gem_spec
  Gem::Specification.new do |spec|
    spec.name = PROJECT.sub(JRUBY_PROJECT_SUFFIX, EMPTY_STRING)
    spec.version = Server::VERSION
    spec.summary = 'TCP Server for JRuby packaged as a gem.'
    spec.description =
      'TCP Server for JRuby is a tcp server which supports a demo echo ' \
      'server application.'
    spec.authors = ['Nels Nelson']
    spec.email = 'nels@nelsnelson.org'
    spec.files = %w[LICENSE Rakefile README.md] + %w[lib bin web].map do |dir|
      Dir[File.join(dir, '**', '*')]
    end.flatten
    spec.bindir = 'exe'
    spec.executables << 'tcp_server'
    spec.platform = 'java'
    spec.homepage = "https://rubygems.org/gems/#{PROJECT}"
    spec.metadata = {
      'source_code_uri' => "https://gitlab.com/nelsnelson/#{PROJECT}",
      'bug_tracker_uri' => "https://gitlab.com/nelsnelson/#{PROJECT}/issues",
      'rubygems_mfa_required' => 'true'
    }
    spec.license = 'MIT'

    spec.require_paths = ['lib']
    spec.required_ruby_version = '>= 2.6.8'
    spec.add_dependency 'apache-log4j-2', '~> 2.17.1'
    spec.add_dependency 'bouncycastle', '~> 1.70.1'
    spec.add_dependency 'netty-io', '~> 4.1.74'
  end
end
# rubocop: enable Gemspec/RequiredRubyVersion
# rubocop: enable Metrics/AbcSize
# rubocop: enable Metrics/MethodLength

GEM_SPEC = gem_spec unless defined?(GEM_SPEC)

GEM_SPEC
